using NUnit.Framework;

namespace ModThree.Test
{
    [TestFixture]
    public class ProgramTests
    {
        private static readonly string[] Null = null;
        private static readonly string[] Empty = { };
        private static readonly string[] Single = { "101" };
        private static readonly string[] Invalid = { "abc" };
        private static readonly string[] Multiple = { "110", "abc" };

        [Test]
        public void GetParam_Null_ReturnsNull()
        {
            string result = "";
            Assert.DoesNotThrow(() =>
            {
                result = Program.GetParam(Null);
            });
            Assert.IsNull(result);
        }

        [Test]
        public void GetParam_Empty_ReturnsNull()
        {
            string result = "";
            Assert.DoesNotThrow(() =>
            {
                result = Program.GetParam(Empty);
            });
            Assert.IsNull(result);
        }

        [Test]
        public void GetParam_Single_ReturnsValue()
        {
            string result = null;
            Assert.DoesNotThrow(() =>
            {
                result = Program.GetParam(Single);
            });
            Assert.IsNotNull(result);
            Assert.AreEqual("101", result);
        }

        [Test]
        public void GetParam_Invalid_ReturnsValue()
        {
            string result = null;
            Assert.DoesNotThrow(() =>
            {
                result = Program.GetParam(Invalid);
            });
            Assert.IsNotNull(result);
            Assert.AreEqual("abc", result);
        }

        [Test]
        public void GetParam_Multiple_ReturnsValue()
        {
            string result = null;
            Assert.DoesNotThrow(() =>
            {
                result = Program.GetParam(Multiple);
            });
            Assert.IsNotNull(result);
            Assert.AreEqual("110", result);
        }
    }
}