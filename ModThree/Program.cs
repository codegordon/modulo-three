﻿using System;

namespace ModThree
{
    internal class Program
    {
        private static ModThree m3 = new ModThree();

        public static void Main(string[] args)
        {
            string param = GetParam(args);
            if (param == null)
            {
                RunSample();
            }
            else
            {
                ModThree(param);
            }
        }

        internal static string GetParam(string[] args)
        {
            if ((args == null) || (args.Length == 0))
            {
                return null;
            }
            if ((args[0] == null) || (args[0].Length == 0))
            {
                return null;
            }
            return args[0];
        }

        internal static void RunSample()
        {
            Console.WriteLine("Syntax: m3 [input]");
            Console.WriteLine(" input: a binary string, to be interpreted as an integer value");
            Console.WriteLine();
            Console.WriteLine("<Examples>");
            ModThree("110");
            ModThree("1010");
        }

        internal static void ModThree(string input)
        {
            int result = m3.Run(input);
            Console.WriteLine("Result for \"" + input + "\": " + result);
        }
    }
}