﻿using FiniteAutomaton;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ModThree
{
    internal class ModThree : FiniteAutomaton<int, int, string>, IFiniteAutomaton<int, int, string>
    {
        public ModThree()
        {
            int[] Q = { 0, 1, 2 };
            int[] Sigma = { 0, 1 };
            int q0 = 0;
            int[] F = { 0, 1, 2 };
            Func<int, int, int> Delta = (int q, int v) =>
                {
                    if ((q == 0 && v == 1) || (q == 1 && v == 0))
                    {
                        return q + 1;
                    }
                    else if ((q == 1 && v == 1) || (q == 2 && v == 0))
                    {
                        return q - 1;
                    }
                    return q;
                };

            ValidInit(Q, Sigma, q0, F, Delta);

            this.Q = Q.ToList().AsReadOnly();
            this.Sigma = Sigma.ToList().AsReadOnly();
            this.Current = q0;
            this.F = F.ToList().AsReadOnly();
            this.Delta = Delta;
        }

        public override ReadOnlyCollection<int> Q { get; protected set; }
        public override ReadOnlyCollection<int> Sigma { get; protected set; }
        public override int Current { get; protected set; }
        public override ReadOnlyCollection<int> F { get; protected set; }
        protected override Func<int, int, int> Delta { get; set; }

        public override int Run(string i)
        {
            if ((i == null) || (i.Length == 0))
            {
                throw new ArgumentException("Input string cannot be null or empty.");
            }
            List<int> l = new List<int>();
            foreach (char c in i)
            {
                int v = Convert.ToInt32(Char.GetNumericValue(c));
                if (!Sigma.Contains(v))
                {
                    throw new ArgumentException("Input string contains an invalid character.");
                }
                l.Add(v);
            }
            int q = Current;
            foreach (int v in l)
            {
                q = Delta(q, v);
            }
            if (!F.Contains(q))
            {
                throw new InvalidOperationException("Final state is not an accepted final state.");
            }
            return q;
        }
    }
}