﻿using System.Collections.ObjectModel;

namespace FiniteAutomaton
{
    public interface IFiniteAutomaton<State, Alpha, Input>
    {
        ReadOnlyCollection<State> Q { get; }
        ReadOnlyCollection<Alpha> Sigma { get; }
        State Current { get; }
        ReadOnlyCollection<State> F { get; }

        State Run(Input i);
    }
}
