﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace FiniteAutomaton
{
    public abstract class FiniteAutomaton<State, Alpha, Input> : IFiniteAutomaton<State, Alpha, Input>
    {
        public abstract ReadOnlyCollection<State> Q { get; protected set; }
        public abstract ReadOnlyCollection<Alpha> Sigma { get; protected set; }
        public abstract State Current { get; protected set; }
        public abstract ReadOnlyCollection<State> F { get; protected set; }
        protected abstract Func<State, Alpha, State> Delta { get; set; }

        protected virtual bool ValidInit
        (
            IEnumerable<State> Q,
            IEnumerable<Alpha> Sigma,
            State q0,
            IEnumerable<State> F,
            Func<State, Alpha, State> Delta
        )
        {
            if ((Q == null) || (Q.Count() == 0))
            {
                return false;
            }
            if ((Sigma == null) || (Sigma.Count() == 0))
            {
                return false;
            }
            if ((q0 == null) || !Q.Contains(q0))
            {
                return false;
            }
            if ((F == null) || (F.Count() == 0) || !F.All(f => Q.Contains(f)))
            {
                return false;
            }
            if ((Delta == null) || !Q.All(q => Sigma.All(s => Q.Contains(Delta(q, s)))))
            {
                return false;
            }
            return true;
        }

        public abstract State Run(Input i);
    }
}
