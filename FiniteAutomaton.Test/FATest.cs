using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace FiniteAutomaton.Test
{
    public class FATest : FiniteAutomaton<int, int, string>, IFiniteAutomaton<int, int, string>
    {
        public FATest
        (
            IEnumerable<int> Q,
            IEnumerable<int> Sigma,
            int q0,
            IEnumerable<int> F,
            Func<int, int, int> Delta
        )
        {
            if (!ValidInit(Q, Sigma, q0, F, Delta))
            {
                throw new ArgumentException("Initialization settings are invalid.");
            }
            this.Q = Q.ToList().AsReadOnly();
            this.Sigma = Sigma.ToList().AsReadOnly();
            this.Current = q0;
            this.F = F.ToList().AsReadOnly();
            this.Delta = Delta;
        }

        public override ReadOnlyCollection<int> Q { get; protected set; }
        public override ReadOnlyCollection<int> Sigma { get; protected set; }
        public override int Current { get; protected set; }
        public override ReadOnlyCollection<int> F { get; protected set; }
        protected override Func<int, int, int> Delta { get; set; }

        public override int Run(string i)
        {
            if ((i == null) || (i.Length == 0))
            {
                throw new ArgumentException("Input string cannot be null or empty.");
            }
            List<int> l = new List<int>();
            foreach (char c in i)
            {
                int v = Convert.ToInt32(Char.GetNumericValue(c));
                if (!Sigma.Contains(v))
                {
                    throw new ArgumentException("Input string contains an invalid character.");
                }
                l.Add(v);
            }
            int q = Current;
            foreach (int v in l)
            {
                q = Delta(q, v);
            }
            if (!F.Contains(q))
            {
                throw new InvalidOperationException("Final state is not an accepted final state.");
            }
            return q;
        }
    }
}