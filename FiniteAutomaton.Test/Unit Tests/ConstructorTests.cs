﻿using NUnit.Framework;
using System;

namespace FiniteAutomaton.Test.UnitTests
{
    [TestFixture]
    public class ConstructorTests
    {
        private static readonly int[] Q = { 0, 1, 2 };
        private static readonly int[] Sigma = { 0, 1 };
        private static readonly int q0 = 0;
        private static readonly int[] F = { 0, 1, 2 };
        private static readonly Func<int, int, int> Delta = (int q, int v) =>
        {
            if ((q == 0 && v == 1) || (q == 1 && v == 0))
            {
                return q + 1;
            }
            else if ((q == 1 && v == 1) || (q == 2 && v == 0))
            {
                return q - 1;
            }
            return q;
        };
        private FATest SUT;

        [Test]
        public void Constructor_NullQ_Throws()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(null, Sigma, q0, F, Delta);
            });
        }

        [Test]
        public void Constructor_EmptyQ_Throws()
        {
            int[] param = { };
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(param, Sigma, q0, F, Delta);
            });
        }

        [Test]
        public void Constructor_NullSigma_Throws()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, null, q0, F, Delta);
            });
        }

        [Test]
        public void Constructor_EmptySigma_Throws()
        {
            int[] param = { };
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, param, q0, F, Delta);
            });
        }

        [Test]
        public void Constructor_InvalidQ0_Throws()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, Sigma, -1, F, Delta);
            });
        }

        [Test]
        public void Constructor_NullF_Throws()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, Sigma, q0, null, Delta);
            });
        }

        [Test]
        public void Constructor_EmptyF_Throws()
        {
            int[] param = { };
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, Sigma, q0, param, Delta);
            });
        }

        [Test]
        public void Constructor_InvalidF_Throws()
        {
            int[] param = { -1, 1 };
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, Sigma, q0, param, Delta);
            });
        }

        [Test]
        public void Constructor_NullDelta_Throws()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, Sigma, q0, F, null);
            });
        }

        [Test]
        public void Constructor_InvalidDelta_Throws()
        {
            Func<int, int, int> param = (int q, int v) =>
                {
                    if ((q == 0 && v == 1) || (q == 1 && v == 0))
                    {
                        return q + 1;
                    }
                    else if ((q == 1 && v == 1) || (q == 2 && v == 0))
                    {
                        return q - 1;
                    }
                    return -1;
                };
            Assert.Throws<ArgumentException>(() =>
            {
                SUT = new FATest(Q, Sigma, q0, F, param);
            });
        }

        [Test]
        public void Constructor_ValidParameters_ReturnsObj()
        {
            Assert.DoesNotThrow(() =>
            {
                SUT = new FATest(Q, Sigma, q0, F, Delta);
            });
            Assert.IsNotNull(SUT);
        }
    }
}