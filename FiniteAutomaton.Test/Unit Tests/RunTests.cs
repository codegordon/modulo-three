﻿using NUnit.Framework;
using System;

namespace FiniteAutomaton.Test.UnitTests
{
    [TestFixture]
    public class RunTests
    {
        private static readonly int[] Q = { 0, 1, 2 };
        private static readonly int[] Sigma = { 0, 1 };
        private static readonly int q0 = 0;
        private static readonly int[] F = { 0, 1, 2 };
        private static readonly Func<int, int, int> Delta = (int q, int v) =>
            {
                if ((q == 0 && v == 1) || (q == 1 && v == 0))
                {
                    return q + 1;
                }
                else if ((q == 1 && v == 1) || (q == 2 && v == 0))
                {
                    return q - 1;
                }
                return q;
            };
        private FATest SUT;

        [SetUp]
        public void SetUp()
        {
            SUT = new FATest(Q, Sigma, q0, F, Delta);
            Assert.IsNotNull(SUT);
        }

        [Test]
        public void Run_0x0_Input0_Returns0()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("0");
            });
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Run_0x0_Input1_Returns1()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("1");
            });
            Assert.AreEqual(1, result);
        }

        [Test]
        public void Run_0x0_Input2_Throws()
        {
            int result = -1;
            Assert.Throws<ArgumentException>(() =>
            {
                result = SUT.Run("2");
            });
        }

        [Test]
        public void Run_0x0_InputA_Throws()
        {
            int result = -1;
            Assert.Throws<ArgumentException>(() =>
            {
                result = SUT.Run("A");
            });
        }

        [Test]
        public void Run_0x00_Input00_Returns0()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("00");
            });
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Run_0x00_Input01_Returns1()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("01");
            });
            Assert.AreEqual(1, result);
        }

        [Test]
        public void Run_0x00_Input10_Returns2()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("10");
            });
            Assert.AreEqual(2, result);
        }

        [Test]
        public void Run_0x00_Input11_Returns0()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("11");
            });
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Run_0x0_Input02_Throws()
        {
            int result = -1;
            Assert.Throws<ArgumentException>(() =>
            {
                result = SUT.Run("02");
            });
        }

        [Test]
        public void Run_0x0_Input0A_Throws()
        {
            int result = -1;
            Assert.Throws<ArgumentException>(() =>
            {
                result = SUT.Run("0A");
            });
        }

        [Test]
        public void Run_0x000_Input000_Returns0()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("000");
            });
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Run_0x000_Input001_Returns1()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("001");
            });
            Assert.AreEqual(1, result);
        }

        [Test]
        public void Run_0x000_Input010_Returns2()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("010");
            });
            Assert.AreEqual(2, result);
        }

        [Test]
        public void Run_0x000_Input011_Returns0()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("011");
            });
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Run_0x000_Input100_Returns1()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("100");
            });
            Assert.AreEqual(1, result);
        }

        [Test]
        public void Run_0x000_Input101_Returns2()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("101");
            });
            Assert.AreEqual(2, result);
        }

        [Test]
        public void Run_0x000_Input110_Returns0()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("110");
            });
            Assert.AreEqual(0, result);
        }

        [Test]
        public void Run_0x000_Input111_Returns1()
        {
            int result = -1;
            Assert.DoesNotThrow(() =>
            {
                result = SUT.Run("111");
            });
            Assert.AreEqual(1, result);
        }

        [Test]
        public void Run_0x0_Input012_Throws()
        {
            int result = -1;
            Assert.Throws<ArgumentException>(() =>
            {
                result = SUT.Run("012");
            });
        }

        [Test]
        public void Run_0x0_Input01A_Throws()
        {
            int result = -1;
            Assert.Throws<ArgumentException>(() =>
            {
                result = SUT.Run("01A");
            });
        }
    }
}